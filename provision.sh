mkdir /home/bargee/containers
mkdir /home/bargee/compose
chown 1000:1000 /home/bargee/compose

pkg install vim
pkg install git
pkg install make

# gitconfig
tee /home/bargee/.gitconfig << EOF
[core]
    filemode = false
    editor = vim
[http]
    sslVerify = false
    postBuffer = 524288000
[alias]
    untrack = rm -r --cached
    pull-dry-run  = !"git fetch origin; B=$(git rev-parse --abbrev-ref HEAD); git diff --stat --summary ${B}..origin/${B}"
    nevermind = !git reset --hard HEAD && git clean -d -f
    precommit = diff --cached --diff-algorithm=minimal -w
    pl  = pull
    pul = pull
    ps  = push
    pus = push
    co  = checkout
    cp  = cherry-pick
    br  = branch
    tg  = tag
    st  = stash
    sta = stash
EOF

# docker engine
/etc/init.d/docker restart 19.03.2

# docker compose
wget -L https://github.com/docker/compose/releases/download/1.24.1/docker-compose-`uname -s`-`uname -m`
chmod +x docker-compose-`uname -s`-`uname -m`
mv docker-compose-`uname -s`-`uname -m` /opt/bin/docker-compose
chown root:root /opt/bin/docker-compose

# bootstrap
tee /etc/init.d/start.sh << EOF
pkg install vim
pkg install git
pkg install make
EOF
chmod +x /etc/init.d/start.sh

# ctop
sudo wget https://github.com/bcicen/ctop/releases/download/v0.7.1/ctop-0.7.1-linux-amd64 -O /opt/bin/ctop
sudo chmod +x /opt/bin/ctop
